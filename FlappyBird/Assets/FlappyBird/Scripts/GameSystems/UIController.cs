using TMPro;
using UnityEngine;
public class UIController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI _score;
    [SerializeField] private PlayerController _player;
    public void UpdateScore()
    {
        _score.text = GetPlayerScore().ToString("0000");
    }
    public void HidePopUp(GameObject target)
    {
        target.gameObject.SetActive(false);
    }
    private int GetPlayerScore()
    {
        if(_player != null)
        {
            return _player.Score;
        }
        return 0;
    }
    private void Start() {
        UpdateScore();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSystem : MonoBehaviour,IMovable
{
    [SerializeField] private SpawnSystemSettings _spawnSystemSettings;
    [SerializeField] private Transform[] spawnsLocation;
    public bool _isPlaying;
    private void Start() {
        _spawnSystemSettings.ResetValues();
    }
    private void FixedUpdate() {
        if (_isPlaying)
        {
            HorizontalMovement();
        }
    }
    #region Spawn Methods
    private void Spawn()
    {
        int chosenObject = Random.Range(0,_spawnSystemSettings.PrefabsFactories.Length);
        GameObject prefab = _spawnSystemSettings.PrefabsFactories[chosenObject].Create();
        AtSpawn(prefab);
        StartTimer(_spawnSystemSettings.SpawnDelay);
    }
    private void AtSpawn(GameObject createdObj)
    {
        SpawnableObjects script = createdObj.GetComponent<SpawnableObjects>();
        if (script != null)
        {
            createdObj.transform.position = GetRightSpawnPosition(script.GetSpawnLocation());
            script.PositionAdjustment();
        }
    }
    private Vector3 GetRightSpawnPosition(string spawnPoint)
    {
        switch (spawnPoint)
        {
            case "Ground":
                return spawnsLocation[0].position;
            case "Middle":
                return spawnsLocation[1].position;
            case "Top":
                return spawnsLocation[2].position;
            default:
                return spawnsLocation[1].position;
        }
    }
    public void StartSpawn()
    {
        _isPlaying = true;
        StartTimer(_spawnSystemSettings.SpawnDelay);
    }
    public void EndSpawn()
    {
        _isPlaying = false;
        StopTimer();
    }
    #endregion
    #region Interface Implementation
    public void HorizontalMovement()
    {
        if (_spawnSystemSettings.IsSpawnSystemMovable)
        {
            transform.position += Vector3.right*_spawnSystemSettings.SystemMovementSpeed*Time.deltaTime;
        }
    }
    public void VerticalMovement()
    {
       throw new System.Exception(); 
    }
    #endregion
    //TODO improve this implementation to follow the single responsability principle
    #region Timer Implementation
    private void StartTimer(float timer)
    {
        _timerRoutine = StartCoroutine(Timer(timer)); 
    }
    private void StopTimer()
    {
        StopCoroutine(_timerRoutine);
    }
    private Coroutine _timerRoutine;
    private IEnumerator Timer(float timeToEnd)
    {
        yield return new WaitForSeconds(timeToEnd);
        Spawn();
    }
    #endregion
}

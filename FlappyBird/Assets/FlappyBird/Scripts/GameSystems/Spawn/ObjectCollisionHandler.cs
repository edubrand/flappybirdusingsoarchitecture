using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCollisionHandler : MonoBehaviour
{
    private int score = 1;
    private void OnCollisionEnter2D(Collision2D other) {
       if (other.gameObject.GetComponent<PlayerController>())
       {
           other.gameObject.GetComponent<PlayerController>().onplayerDeath.Invoke();
       }
    }
    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.GetComponent<PlayerController>())
        {
           other.gameObject.GetComponent<PlayerController>().IncreaseScore(score);
           this.GetComponent<SpawnableObjects>().Destruction();
        }
    }
}

using UnityEngine;

public class SpawnableObjects : MonoBehaviour
{
    
    [SerializeField] private Location _spawnableObjectLocation;
    [SerializeField] private ObjectType _objectType;
    [SerializeField] private SpawnableObjectsBalanceSettings _balanceSettings;
    [SerializeField] private GameObject _groupObject;
    [SerializeField] private GameObject upPart;
    [SerializeField] private GameObject downPart;
    [SerializeField] private bool _isPlaying = true;

    public string GetSpawnLocation()
    {   
        return _spawnableObjectLocation.ToString();
    }
    public void PositionAdjustment()
    {
        if(_groupObject != null)
        {
            _balanceSettings.AdjustOffset(_groupObject);
        }
        if (upPart != null && downPart != null)
        {
            _balanceSettings.AdjustVerticalDistanceBetweenParts(upPart,downPart);
        }
    }
    public void ChangeState(bool playing)
    {
        _isPlaying = playing;
    }
    public void Destruction()
    {
        if (_isPlaying)
        {
            Destroy(this.gameObject,3);
        }
    }
    public enum ObjectType
    {
        Collectable,
        Obstacle,
        Destructable
    }
    public enum Location
    {
        Ground,
        Middle,
        Sky,
        
    }
}

using UnityEngine;
using UnityEngine.Events;
public class InputController : MonoBehaviour
{
    public UnityEvent firstInput;
    public UnityEvent tapInput;
    private bool isFirstInput = true;
    public bool inputAllowable = true;
    private void Update() {
        if (inputAllowable)
        {
            TapInput();
        }
    }
    private void TapInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (isFirstInput)
            {
                firstInput.Invoke();
                isFirstInput = false;
            }
            tapInput.Invoke();
        }
    }
    public void AllowInput(bool allow)
    {
        inputAllowable = allow;
    }
}

using UnityEngine;
using UnityEngine.Events;
public class PlayerMovement : IMovable
{
    private readonly PlayerSettings _playerSettings;
    private Rigidbody2D _rb;
    //Construct
    public PlayerMovement(PlayerSettings playerSettings, Rigidbody2D rb)
    {
        _playerSettings = playerSettings;
        _rb = rb;
    }
    public void HorizontalMovement()
    {
        _rb.velocity = new Vector2(_playerSettings.MovementSpeed,_rb.velocity.y);
    }
    public void VerticalMovement()
    {
        ResetVelocity("vertical");
        _rb.AddForce(new Vector2(0,_playerSettings.FlapWingStregth),ForceMode2D.Impulse);
    }
    private void ResetVelocity(string axis)
    {
        switch (axis)
        {
            case "vertical":
                _rb.velocity = new Vector2(_rb.velocity.x,0);
                break;
            case "horizontal":
                _rb.velocity = new Vector2(0,_rb.velocity.y);
                break;
            default:
                _rb.velocity = Vector2.zero;
                break;
        }
    }
}

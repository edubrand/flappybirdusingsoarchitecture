using UnityEngine;
using UnityEngine.Events;
[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] PlayerSettings _playerSettings;
    public UnityEvent onplayerDeath;
    public UnityEvent onPlayerGetScore;
    private Rigidbody2D _rb;
    private PlayerMovement _playerMovement;
    private InputController _inputController;
    public bool _isPlaying;
    private int _score;
    public int Score {get{return _score;}}
    private void Awake() {
        _rb = GetComponent<Rigidbody2D>();
        if (_isPlaying)
        {
            _rb.gravityScale = _playerSettings.GravityScale;
        }
        else
        {
            _rb.gravityScale = 0;
        }
        _playerMovement = new PlayerMovement(_playerSettings,_rb);
    }
    private void FixedUpdate() {
        if (_isPlaying)
        {
            _playerMovement.HorizontalMovement();
        }
    }
    public void FlapWing()
    {
        if (_isPlaying)
        {
            _playerMovement.VerticalMovement();
        }
    }
    public void ChangeState(bool isPlayable)
    {
        _isPlaying = isPlayable;
        if (_isPlaying)
        {
            _rb.gravityScale = _playerSettings.GravityScale;
        }
    }
    public void IncreaseScore(int value)
    {
        if (_isPlaying)
        {
            _score += 1;
            onPlayerGetScore.Invoke();
        }
    }
}

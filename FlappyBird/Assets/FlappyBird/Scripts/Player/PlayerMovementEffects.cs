using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementEffects : MonoBehaviour,IRotatable
{
    [SerializeField] PlayerEffectSettings _effectSettings;
    private Rigidbody2D _rb;
    public bool _isPlaying = false;
    private void Awake() {
        _rb = GetComponent<Rigidbody2D>();
    }
    public void Rotation(GameObject rotationTarget)
    {
        if (_rb.velocity.y>0)
        {
            rotationTarget.transform.rotation = Quaternion.Euler(0,0,Mathf.Lerp(rotationTarget.transform.rotation.z,_effectSettings.RotationAngle,_effectSettings.LerpParameter));
        }
        else
        {
            rotationTarget.transform.rotation = Quaternion.Euler(0,0,Mathf.Lerp(rotationTarget.transform.rotation.z,-_effectSettings.RotationAngle,_effectSettings.LerpParameter));
        }
    }
    private void FixedUpdate() {
        if (_isPlaying)
        {
            Rotation(this.gameObject);
        }
    }
    public void ChangeState(bool isPlaying)
    {
        _isPlaying = isPlaying;
    }
}

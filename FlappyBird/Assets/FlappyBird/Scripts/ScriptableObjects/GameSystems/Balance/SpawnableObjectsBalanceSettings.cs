using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "SpawnableObjectsBalanceSettings",menuName = "Game System Settings/Spawnable Objects Balance Settings")]

public class SpawnableObjectsBalanceSettings : ScriptableObject
{
    [SerializeField] private float _minimumHeigthOffset;
    [SerializeField] private float _maximumHeigthOffset;
    [SerializeField] private float _maximumDistanceBetweenParts;
    [SerializeField] private float _minimumDistanceBetweenParts;

    public void AdjustOffset(GameObject _objectToTranslate)
    {
        float value = Random.Range(_minimumHeigthOffset,_maximumHeigthOffset);
        _objectToTranslate.transform.position += new Vector3(0,value,0);
    }
    public void AdjustVerticalDistanceBetweenParts(GameObject upPart,GameObject downPart)
    {
        float value = Random.Range(_minimumDistanceBetweenParts,_maximumDistanceBetweenParts);
        upPart.transform.position -= new Vector3(0,value,0);
        downPart.transform.position += new Vector3(0,value,0);

    }

}

using UnityEngine;
[CreateAssetMenu(fileName = "New Spawn System",menuName = "Game System Settings/Spawn System Settings")]
public class SpawnSystemSettings : ScriptableObject
{
    [SerializeField] private bool _initializeSpawnOnStart;
    [SerializeField] private bool _isSpawnSystemMovable;
    [SerializeField] private FloatReference _systemMovementSpeed;
    [SerializeField] private FloatReference _baseSpawnDelay;
    [SerializeField] private FloatVariable _spawnDelay;
    [SerializeField] private GameObjectFactorySO[] _prefabsFactories;

    public bool InitializeSpawnOnStart {get{return _initializeSpawnOnStart;}}
    public bool IsSpawnSystemMovable {get{return _isSpawnSystemMovable;}}
    public float SystemMovementSpeed {get{return _systemMovementSpeed.Value;}}
    public float SpawnDelay {get{return _spawnDelay.value;}}
    public GameObjectFactorySO[] PrefabsFactories {get{return _prefabsFactories;}}

    public void ResetValues()
    {
        _spawnDelay.SetValue(_baseSpawnDelay.Value);
    }
}

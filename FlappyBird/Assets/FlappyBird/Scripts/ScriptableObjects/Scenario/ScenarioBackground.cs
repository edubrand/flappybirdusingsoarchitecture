using UnityEngine;
using System.Collections.Generic;
[CreateAssetMenu(menuName = "Scenario/Background", fileName = "ScrollingParts")]
public class ScenarioBackground : ScriptableObject
{
    #if UNITY_EDITOR
        [Multiline]
        [SerializeField] private string developerDescription;
    #endif
    [Tooltip("Scenario part you want to be constructed")]
    [SerializeField] public GameObject[] scrollingParts;
    
    [Tooltip("Width x Heigth in Unity")]
    [SerializeField] public Vector2[] _sizeOfBackgroundParts;

    private List<GameObject> partsInWorld;
    public List<GameObject> PartsInWorld {get{return partsInWorld;}}

    public void AddPartReference(GameObject part)
    {
        partsInWorld.Add(part);
    }
    public void RemovePartReference(GameObject part)
    {
        partsInWorld.Remove(part);
    }
    public void CleanReferences()
    {
        partsInWorld.Clear();
        partsInWorld = new List<GameObject>();
    }

}

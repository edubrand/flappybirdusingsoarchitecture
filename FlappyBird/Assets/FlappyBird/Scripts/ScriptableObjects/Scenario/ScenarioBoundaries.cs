using UnityEngine;
[CreateAssetMenu(menuName = "Scenario/Boundaries", fileName = "BoundsParts")]
public class ScenarioBoundaries : ScriptableObject
{
    [SerializeField] GameObject[] boundariesParts;
    
}

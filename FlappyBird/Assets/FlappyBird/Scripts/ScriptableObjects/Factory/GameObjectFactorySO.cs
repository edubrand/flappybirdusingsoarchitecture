using UnityEngine;
[CreateAssetMenu(fileName = "New Gameobject Factory",menuName = "Factory/GameObject Factory")]
public class GameObjectFactorySO : FactorySO<GameObject>
{
    public GameObject prefab;
    public override GameObject Create()
    {
        return Instantiate(prefab);
    }

}

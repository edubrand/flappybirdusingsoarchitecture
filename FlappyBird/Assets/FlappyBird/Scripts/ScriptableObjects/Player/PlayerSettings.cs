using UnityEngine;
[CreateAssetMenu(menuName = "Player/PlayerSettings", fileName = "PlayerData")]
public class PlayerSettings : ScriptableObject
{
    #if UNITY_EDITOR
        [Multiline]
        [SerializeField] private string developerDescription;
    #endif
    [Header("Balance Variables")]
    [SerializeField] FloatReference movementSpeed;
    [SerializeField] FloatReference flapWingStregth;
    [SerializeField] FloatReference gravityScale;
    public float MovementSpeed {get {return movementSpeed.Value;}}
    public float FlapWingStregth {get {return flapWingStregth.Value;}}
    public float GravityScale {get {return gravityScale.Value;}}

}

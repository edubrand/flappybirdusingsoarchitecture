using UnityEngine;
[CreateAssetMenu(menuName = "Player/PlayerEffectSettings", fileName = "PlayerEffectData")]
public class PlayerEffectSettings : ScriptableObject
{
    #if UNITY_EDITOR
        [Multiline]
        [SerializeField] private string developerDescription;
    #endif
    [SerializeField] FloatReference _rotationEffectAngle;
    [SerializeField] FloatReference _lerpParameter;
    
    public float RotationAngle {get{return _rotationEffectAngle.Value;}}
    public float LerpParameter {get{return _lerpParameter.Value;}}
    
}

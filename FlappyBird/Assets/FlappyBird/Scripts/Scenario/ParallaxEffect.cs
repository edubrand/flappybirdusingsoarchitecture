using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxEffect : MonoBehaviour
{
    [Tooltip("Parallax speed for each background Note: Each position of the array represents the same position as the background array")]
    [SerializeField] private FloatReference[] _paralaxxSpeed;
    [SerializeField] private Vector3[] _backgroundMoveDirection;
    [Tooltip("Background that will be applied the parallax effect")]
    [SerializeField] private ScenarioBackground[] _backgrounds;
    public bool _isPlaying; 
    private void Paralaxx()
    {
        int index = -1;
        foreach (ScenarioBackground backgroundLayer in _backgrounds)
        {
            index += 1;
            Vector2 moveDir = _backgroundMoveDirection[index];
            foreach (var backgroundPart in backgroundLayer.PartsInWorld)
            {
                backgroundPart.transform.position += _backgroundMoveDirection[index]*_paralaxxSpeed[index].Value*Time.deltaTime;
            }
        }
    }
    private void FixedUpdate() {
        if (_isPlaying)
        {
            Paralaxx();
        }
    }
    public void ChangeState(bool isPlayable)
    {
        _isPlaying = isPlayable;
    }
}

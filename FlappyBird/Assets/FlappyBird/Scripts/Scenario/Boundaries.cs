using UnityEngine;

public class Boundaries : MonoBehaviour
{
    private Collider2D _collider;
    [SerializeField] private FloatReference pushBackForce;
    private void Awake() {
        _collider = GetComponent<Collider2D>();
    }
    private void OnTriggerEnter2D(Collider2D other) 
    {
        Rigidbody2D rb = other.GetComponent<Rigidbody2D>();
        if (rb != null)
        {
            PushBackObject(rb);
        }
    }
    private void PushBackObject(Rigidbody2D targetRb)
    {
        targetRb.velocity = new Vector2(targetRb.velocity.x,0);
        targetRb.AddForce(new Vector2(0,-pushBackForce.Value),ForceMode2D.Impulse);
    }
}

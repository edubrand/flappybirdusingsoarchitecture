using UnityEngine;
using UnityEngine.Events;

public class ScenarioConstruction : MonoBehaviour
{
    [Tooltip("Put the data of scrolling background")]
    [SerializeField] private ScenarioBackground _scrollingBackground;
    [SerializeField] private Vector2 _constructionStartPointOffset;
    private Vector2 _cameraStartPosition;
    private void Start() {
        CleanScenarioPartsReferences();
        ConstructParts(Axis.Horizontal);
    }
    ///</summary> 
        //based on camera position
    ///</summary> 
    public void GetConstructStartPoint(GameObject startPoint)
    {
        _cameraStartPosition = startPoint.transform.position;
    }
    private void ConstructParts(Axis constructAxis)
    {
        if (constructAxis == Axis.Horizontal)
        {
            GameObject[] parts = _scrollingBackground.scrollingParts;
            Vector2 currentPosition = _cameraStartPosition + _constructionStartPointOffset;
            for (int i = 0; i < parts.Length; i++)
            {
                GameObject part = Instantiate(parts[i],currentPosition,Quaternion.identity,this.transform);
                _scrollingBackground.AddPartReference(part);
                currentPosition +=  new Vector2(_scrollingBackground._sizeOfBackgroundParts[i].x,0);
            }
        }
        else
        {
            GameObject[] parts = _scrollingBackground.scrollingParts;
            Vector2 currentPosition = _cameraStartPosition;
            for (int i = 0; i < parts.Length; i++)
            {
                GameObject part = Instantiate(parts[i],currentPosition,Quaternion.identity,this.transform);
                _scrollingBackground.AddPartReference(part);
                currentPosition +=  new Vector2(0,_scrollingBackground._sizeOfBackgroundParts[i].y);
            }
        }
    }

    private void CleanScenarioPartsReferences()
    {
        _scrollingBackground.CleanReferences();
    }
    enum Axis
    {
        Horizontal,
        Vertical,
    }
}

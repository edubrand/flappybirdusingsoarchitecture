using UnityEngine;

public interface IRotatable
{
    void Rotation(GameObject rotationTarget);

}

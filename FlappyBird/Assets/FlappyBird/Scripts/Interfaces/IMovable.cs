using UnityEngine;

public interface IMovable
{
    void HorizontalMovement();
    void VerticalMovement();
}

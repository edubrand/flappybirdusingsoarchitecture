using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class InfiniteScrolling : MonoBehaviour
{
    [Tooltip("Put the data of scrolling background")]
    [SerializeField] private ScenarioBackground _scrollingBackground;
    public UnityEvent<GameObject> cameraStartPosition;
    private int _currentPartIndex;
    private int _targetPartIndex;
    private int _lastPartIndex;
    private float _distance;
    private void Start() {
        cameraStartPosition.Invoke(this.gameObject);
        SetPartIndexes();
    }
    private void FixedUpdate() {
        CalculateDistance();
        CheckDistance();
    }
    //Horizontal Only
    private void CalculateDistance()
    {
        _distance = _scrollingBackground.PartsInWorld[_targetPartIndex].transform.position.x - transform.position.x;
    }
    private void CheckDistance()
    {
        if (_distance <= 0)
        {
            RepositionParts();
            ChangeCurrentIndexes();
        }
    }
    private void SetPartIndexes()
    {
        _currentPartIndex = 0;
        _targetPartIndex = _currentPartIndex +1;
        _lastPartIndex = _scrollingBackground.scrollingParts.Length-1;
    }
    private void ChangeCurrentIndexes()
    {
        int temp = _currentPartIndex;
        _currentPartIndex = _targetPartIndex;
        _lastPartIndex = temp;
        _targetPartIndex += 1;
        if (_targetPartIndex % _scrollingBackground.scrollingParts.Length == 0)
        {
            _targetPartIndex = 0;
        }
    }
    private void RepositionParts()
    {
        
        Transform _currentPart = _scrollingBackground.PartsInWorld[_currentPartIndex].transform;
        Transform _lastPart = _scrollingBackground.PartsInWorld[_lastPartIndex].transform;
        _currentPart.position = new Vector2(_lastPart.position.x+_scrollingBackground._sizeOfBackgroundParts[_lastPartIndex].x,_currentPart.position.y);
    }
}

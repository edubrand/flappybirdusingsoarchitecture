using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour, IFollowable
{
    private Vector3 _offset;
    [Tooltip ("Place the gameobject you want to follow")]
    [SerializeField] private GameObject _target;
    
    [Tooltip("Put the speed you want the camera move to the target.")]
    [Range(1,100)]
    public float smoothspeed;
    public bool _isPlaying;

    private void Awake() {
        //If instance scene search for target
        if (_target == null)
        {
            //TODO search for target on scene
        }
        _offset = this.transform.position - _target.transform.position;
    }
    private void FixedUpdate() {
        if (_isPlaying)
        {
            FollowTarget();
        }
    }
    public void FollowTarget()
    {
        Vector3 newPosition = new Vector3(_target.transform.position.x+_offset.x,this.transform.position.y,this.transform.position.z);
        Vector3 smoothedPosition = Vector3.Lerp(transform.position,newPosition,smoothspeed*Time.deltaTime);
        this.transform.position = smoothedPosition;
    }
    public void ChangeState(bool isPlayable)
    {
        _isPlaying = isPlayable;
    }
}

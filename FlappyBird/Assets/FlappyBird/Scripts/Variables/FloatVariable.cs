using UnityEngine;
[CreateAssetMenu]
public class FloatVariable : ScriptableObject
{
    #if UNITY_EDITOR
        [Multiline]
        [SerializeField] private string developerDescription;
    #endif
    public float value;
    public void SetValue(float value)
    {
        this.value = value;
    }
    public void ApplyChange(float amount)
    {
        value += amount;
    }

}
